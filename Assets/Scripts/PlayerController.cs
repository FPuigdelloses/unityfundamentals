﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    private NavMeshAgent agent;
    private Animator anim;
    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent < Animator>();
        agent = GetComponent<NavMeshAgent>();
    }
    void Update()
    {
        anim.SetFloat("Speed", agent.velocity.magnitude);
    }
}
